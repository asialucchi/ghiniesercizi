#!/bin/bash

N="\e[0m"
B="\e[1m"
D="\e[2m"
U="\e[4m"
R="\e[7m"

echo -e "
${B}07 QUOTES${N}

${B}\`${N}${U}command${N}${B}\`${N} 	Sostituisce il risultato del comando
${B}\$(${N}${U}command${N}${B})${N} 	Sostituisce il risultato del comando
${B}\"${N}${U}string${N}${B}\"${N} 	Impedisce la sostituzione di: wildcards, spazi 
${B}'${N}${U}string${N}${B}'${N} 	Impedisce la sostituzione di: wildcards, command substitution, variable substitution, spazi
"

BR=$(cat "$0" | grep -n "# ESEMPIO" | tail -n 1)
BR=${BR%%:*}
ER=$(cat "$0" | grep -n "# FINE ESEMPIO" | tail -n 1)
ER=${ER%%:*}
TR=$(wc -l "$0" | tail -n 1)
TR=${TR%% *}
echo -e "${B}Esempio:${N}"
echo -e "${U}Codice:${N}"
tail -n $((${TR}-${BR})) "$0" | head -n $((${ER}-${BR}-1))
echo
echo -e "${U}Output:${N}"

# ESEMPIO
echo Dear ${USER} the date is: `date` ; echo "Have a nice day!"
# FINE ESEMPIO
echo
