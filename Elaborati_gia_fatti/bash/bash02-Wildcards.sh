#!/bin/bash

N="\e[0m"
B="\e[1m"
D="\e[2m"
U="\e[4m"
R="\e[7m"

echo -e "
${B}02 WILDCARDS${N}

${B}*${N} 		Può essere sostituito da una qualunque sequenza di caratteri, anche vuota
${B}?${N} 		Può essere sostituito da esattamente un singolo carattere
${B}[${N}${U}elenco${N}${B}]${N} 	Può essere sostituito da un solo carattere tra quelli specificati in elenco
"

BR=$(cat "$0" | grep -n "# ESEMPIO" | tail -n 1)
BR=${BR%%:*}
ER=$(cat "$0" | grep -n "# FINE ESEMPIO" | tail -n 1)
ER=${ER%%:*}
TR=$(wc -l "$0" | tail -n 1)
TR=${TR%% *}
echo -e "${B}Esempio:${N}"
echo -e "${U}Codice:${N}"
tail -n $((${TR}-${BR})) "$0" | head -n $((${ER}-${BR}-1))
echo
echo -e "${U}Output:${N}"

# ESEMPIO
ls *Wildcards.sh
ls ?ash02-Wildcards.sh
ls bash[012]2-Wildcards.sh
ls [abc]ash02-Wildcards.sh
ls bash0[0-9]-Wildcards.sh
ls [a-z]ash02-Wildcards.sh
ls bash0[[:digit:]]-Wildcards.sh
ls bash02-[[:upper:]]ildcards.sh
ls [[:lower:]]ash02-Wildcards.sh
# FINE ESEMPIO
echo
