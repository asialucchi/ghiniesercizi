#!/bin/bash

N="\e[0m"
B="\e[1m"
D="\e[2m"
U="\e[4m"
R="\e[7m"

echo -e "
${B}14 PROCESSI IN BACKGROUD E FOREGROUND${N}

${U}program${N}${B} &${N} 				Lancia un processo direttamente in background
${B}\$!${N} 					Variabile che contiene il pid del processo appena lanciato
${B}\$?${N} 					Variabile che contiene l'exit status del comando più recente

${B}Ctrl+Z${N} 					Sospende un processo in foreground
${B}Ctrl+C${N} 					Termina un processo in foreground

${B}jobs${N} 					Produce una lista numerata dei processi in background
${B}%%${N} 					Identifica il job corrente

${B}bg${N} 					Riprende l'esecuzione di un processo sospeso
${B}fg${N} %n 					Porta in foreground un processo sospeso
${B}kill${N} %n 				Elimina il processo specificato
${B}disown${N} %n 				Sgancia il job dalla shell interattiva che lo ha generato
${B}nohup${N} ${U}program${N}${B}${N} >/dev/null 2>&1 ${B}&${N} 	Lancia un processo direttamente in background sganciandolo dalla shell
"
