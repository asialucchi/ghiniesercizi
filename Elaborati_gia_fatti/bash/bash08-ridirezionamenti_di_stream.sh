#!/bin/bash

N="\e[0m"
B="\e[1m"
D="\e[2m"
U="\e[4m"
R="\e[7m"

echo -e "
${B}08 RIDIREZIONAMENTI DI STREAM${N}

standard input (stdin) 0
standard output (stdout) 1
standard error (stderr) 2

${U}program${N}${B} < ${N}${U}input_file${N} 			Ricevere input da file
${U}program${N}${B} > ${N}${U}output_file${N} 			Ridireziona lo stdout di un programma verso un file sovrascrivendo il vecchio contenuto
${U}program${N}${B} >> ${N}${U}output_file${N} 			Ridireziona lo stdout di un programma verso un file aggiungendolo al vecchio contenuto del file
${U}program1${N}${B} | ${N}${U}program2${N} 			Ridireziona lo stdout di un programma nell'\'' input di un altro programma

${U}program${N}${B} &> ${N}${U}output_and_error_file${N} 	Ridireziona assieme std output e std error su uno stesso file sovrascrivendo il vecchio contenuto
${U}program${N}${B} 2> ${N}${U}error_file${N}>${U}output_file${N} 	Ridireziona std ouput e std error su due diversi file sovrascrivendo il vecchio contenuto

${U}program1${N}${B} 2>&1| ${N}${U}program2${N} 		Ridireziona assieme stdout e stderr di un programma nello stdin di un altro programma
${U}program1${N}${B} |& ${N}${U}program2${N} 			Ridireziona assieme stdout e stderr di un programma nello stdin di un altro programma

${U}program${N}${B} N> ${N}${U}output_file${N} 			Ridireziona il file descriptor N su un file
${U}program${N}${B} N>&M${N} 				Ridireziona il file descriptor N sul file descriptor M
${U}program${N}${B} <N ${N}${U}input_file${N} 			Ridireziona un file sul file descriptor N del programma specificato
"
