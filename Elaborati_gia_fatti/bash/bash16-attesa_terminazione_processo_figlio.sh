#!/bin/bash

N="\e[0m"
B="\e[1m"
D="\e[2m"
U="\e[4m"
R="\e[7m"

echo -e "
${B}16 ATTESA TERMINAIOZNE PROCESSO FIGLIO${N}

${U}program${N}${B} &${N}
${B}wait${N} \$!  	Attende terminazione di program e restituisce il suo exit status
"

BR=$(cat "$0" | grep -n "# ESEMPIO" | tail -n 1)
BR=${BR%%:*}
ER=$(cat "$0" | grep -n "# FINE ESEMPIO" | tail -n 1)
ER=${ER%%:*}
TR=$(wc -l "$0" | tail -n 1)
TR=${TR%% *}
echo -e "${B}Esempio:${N}"
echo -e "${U}Codice:${N}"
tail -n $((${TR}-${BR})) "$0" | head -n $((${ER}-${BR}-1))
echo
echo -e "${U}Output:${N}"

# ESEMPIO
(sleep 2; ls ?.txt) &
wait $!
echo "Il valore restituito da wait è: $?"
# FINE ESEMPIO
echo
