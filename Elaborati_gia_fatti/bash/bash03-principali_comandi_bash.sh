#!/bin/bash

N="\e[0m"
B="\e[1m"
D="\e[2m"
U="\e[4m"
R="\e[7m"

echo -e "
${B}03 PRINCIPALI COMANDI BASH${N}

${B}pwd${N} 			Mostra directory di lavoro corrente
${B}cd${N} [${U}directory${N}] 		Cambia la directory di lavoro corrente
${B}mkdir${N} ${U}directory${N} 	Crea una nuova directory nel percorso specificato
${B}rmdir${N} ${U}directory${N} 	Elimina la directory specificata, se  è vuota
${B}ls${N} -alh [${U}directory${N}] 	Stampa informazioni su tutti i files contenuti nel percorso
${B}rm${N} [${U}FILE${N}] 		Elimina il file specificato
${B}echo${N} [${U}STRING${N}] 		Visualizza in output la sequenza di caratteri specificata
${B}cat${N} [${U}FILE${N}] 		Visualizza in output il contenuto del file specificato
${B}env${N} 			Visualizza le variabili ed il loro valore
${B}which${N} ${U}filename${N} 		Visualizza il percorso in cui si trova (solo se nella variabile PATH) l’eseguibile
${B}mv${N} ${U}SOURCE${N} ${U}DEST${N} 		Sposta il file specificato in una nuova posizione 
${B}ps${N} aux 			Stampa informazioni sui processi in esecuzione
${B}du${N} [${U}FILE${N}] 		Visualizza l’occupazione del disco.
${B}kill${N} -9  <pid> 		Elimina processo avente identificativo pid
${B}killall${N} ${U}name${N} 		Elimina tutti i processi col nome name
${B}bg${N} 			Ripristina un job fermato e messo in sottofondo
${B}fg${N} 			Porta il job più recente in primo piano
${B}df${N} 			Mostra spazio libero dei filesystem montati
${B}touch${N} ${U}FILE${N} 		Crea il file specificato se non esiste, oppure ne aggiorna data.
${B}more${N} ${U}file${N} 		Mostra il file specificato un poco alla volta 
${B}head${N} [${U}FILE${N}] 		Mostra le prime 10 linee del file  specificato
${B}tail${N} [${U}FILE${N}] 		Mostra le ultime 10 linee del file specificato
${B}man${N} ${U}page${N} 		È il manuale, fornisce informazioni sul comando specificato
${B}find${N} 			Cercare dei files
${B}grep${N} 			Cerca tra le righe di file quelle che contengono alcune parole
${B}read${N} ${U}var${N} 		Legge input da standard input e lo inserisce nella variabile specificata
${B}wc${N} [${U}FILE${N}] 		Conta il numero di parole o di caratteri di un file
"
