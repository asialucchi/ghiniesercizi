#!/bin/bash

N="\e[0m"
B="\e[1m"
D="\e[2m"
U="\e[4m"
R="\e[7m"

echo -e "
${B}10 LETTURA DA STANDARD INPUT${N}
"

BR=$(cat "$0" | grep -n "# ESEMPIO" | tail -n 1)
BR=${BR%%:*}
ER=$(cat "$0" | grep -n "# FINE ESEMPIO" | tail -n 1)
ER=${ER%%:*}
TR=$(wc -l "$0" | tail -n 1)
TR=${TR%% *}
echo -e "${B}Esempio:${N}"
echo -e "${U}Codice:${N}"
tail -n $((${TR}-${BR})) "$0" | head -n $((${ER}-${BR}-1))
echo
echo -e "${U}Output:${N}"

# ESEMPIO
echo "Scrivi e premi invio"
echo "Premi Ctrl+D per terminare"
while (( 1 ))
do
	read ROW
	if (( "$?" != 0 )) 
	then
		break
	else
		echo "Letto \"${ROW}\" da stdin"
	fi
done
# FINE ESEMPIO
echo
