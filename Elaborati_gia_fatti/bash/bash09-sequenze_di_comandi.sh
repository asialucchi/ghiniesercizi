#!/bin/bash

N="\e[0m"
B="\e[1m"
D="\e[2m"
U="\e[4m"
R="\e[7m"

echo -e "
${B}09 SEQUENZE DI COMANDI${N}

${U}program1${N}${B} ; ${N}${U}program2${N} 		Sequenza non condizionale
${U}program1${N}${B} || ${N}${U}program2${N} 		Sequenza condizionale con il secondo che viene eseguito solo se il primo fallisce
${U}program1${N}${B} && ${N}${U}program2${N} 		Sequenza condizionale con il secondo che viene eseguito solo se il primo non fallisce
${B}(${N}${U}program1${N}${B} ; ${N}${U}program2${N}${B})${N} 		Raggruppamento di comandi
"
