/* Problema dei filosofi a cena */

#ifndef _thread_SAFE
#define _thread_SAFE
#endif
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#else
#if _POSIX_C_SOURCE < 200112L
#undef  _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <pthread.h> 
#include <inttypes.h>

#include "printerror.h"
#include "DBGpthread.h"

#define NUMPHILOSOPHERS 5

#define THINKING 0
#define HUNGRY 1
#define EATING 2

/* dati da proteggere */
int philosopherState[NUMPHILOSOPHERS];

/* variabili per la sincronizzazione */
pthread_mutex_t mutex;
pthread_cond_t condPhilosopher[NUMPHILOSOPHERS];

int leftPhilosopherIndex(int index) {
	return ((index + 1) % NUMPHILOSOPHERS);
}

int rightPhilosopherIndex(int index) {
	if (index == 0) {
		return(NUMPHILOSOPHERS - 1);
	}
	else {
		return((index - 1) % NUMPHILOSOPHERS);
	}
}

int isTurn(int index) {
	if (philosopherState[index] == HUNGRY
			&& philosopherState[rightPhilosopherIndex(index)] != EATING
			&& philosopherState[leftPhilosopherIndex(index)] != EATING) {
		return 1;
	}
	else {
		return 0;
	}
}

int check() {
	int numEatingPhilosopher = 0;
	int i = 0;

	for (i = 0; i < NUMPHILOSOPHERS; ++i)
	{
		if (philosopherState[i] == EATING)
		{
			numEatingPhilosopher++;
		}
	}
	if (numEatingPhilosopher <= NUMPHILOSOPHERS / 2) {
		return 1;
	}
	return 0;
}

void *philosopher(void *arg)
{
	intptr_t index = (intptr_t)arg;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread philosopher %lu"PRIiPTR, (unsigned long int) index);

	while (1)
	{
		printf("Il thread philosopher %"PRIiPTR" sta pensando \n", index);
		fflush(stdout);
		sleep(1);

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		/* INIZIO SEZIONE CRITICA Eating */
		/* imposta lo stato */
		philosopherState[index] = HUNGRY;
		/*verifica se è il turno del thread corrente */
		if (isTurn(index)) {
			/* è il turno del thread corrente */
			philosopherState[index] = EATING;
		}
		else {
			/* non è il turno del thread corrente, quindi il thread corrente aspetta */
			DBGpthread_cond_wait(&(condPhilosopher[index]), &mutex, label);
		}
		/* verifica di coerenza */
		if (!check()) {
			printf("Il thread philosopher %"PRIiPTR" rileva una incoerenza \n", index);
			fflush(stdout);
			exit(1);
		}
		/* FINE SEZIONE CRITICA Eating */

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);

		printf("Il thread philosopher %"PRIiPTR" sta mangiando \n", index);
		fflush(stdout);
		sleep(1);

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		/* INIZIO SEZIONE CRITICA Thinking */
		/* imposta lo stato */
		philosopherState[index] = THINKING;
		/* segnala agli altri thread che è il loro turno */
		if (isTurn(rightPhilosopherIndex(index))) {
			philosopherState[rightPhilosopherIndex(index)] = EATING;
			DBGpthread_cond_signal(&(condPhilosopher[rightPhilosopherIndex(index)]), label);
		}
		if (isTurn(leftPhilosopherIndex(index))) {
			philosopherState[leftPhilosopherIndex(index)] = EATING;
			DBGpthread_cond_signal(&(condPhilosopher[leftPhilosopherIndex(index)]), label);
		}
		/* verifica di coerenza */
		if (!check()) {
			printf("Il thread philosopher %"PRIiPTR" rileva una incoerenza \n", index);
			fflush(stdout);
			exit(1);
		}
		/* FINE SEZIONE CRITICA Thinking */

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);
	}
	pthread_exit(NULL);
}

int main(void)
{
	pthread_t tid;
	int  rc;
	intptr_t i;

	/* inizializzazione mutex */
	DBGpthread_mutex_init(&mutex, NULL, "mutex_init");

	/* inizializzazione cond */
	for (i = 0; i < NUMPHILOSOPHERS; i++) {
		rc = pthread_cond_init(&(condPhilosopher[i]), NULL);
		if (rc) PrintERROR_andExit(rc, "pthread_cond_init failed");
	}

	/* imposta le condizioni iniziali */
	for (i = 0; i < NUMPHILOSOPHERS; i++) {
		philosopherState[i] = THINKING;
	}

	/* crea i thread */
	for (i = 0; i < NUMPHILOSOPHERS; i++) {
		rc = pthread_create(&tid, NULL, philosopher, (void*)i);
		if (rc) PrintERROR_andExit(rc, "pthread_create failed");
	}

	/* i thread sono in loop infinito */
	pthread_exit(NULL);
	return(0);
}
