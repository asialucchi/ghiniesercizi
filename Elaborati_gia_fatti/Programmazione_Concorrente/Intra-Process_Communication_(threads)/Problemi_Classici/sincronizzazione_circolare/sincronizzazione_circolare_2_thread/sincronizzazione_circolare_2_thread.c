/* Sincronizzazione circolare 2 thread */

#ifndef _THREAD_SAFE
#define _THREAD_SAFE
#endif
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#else
#if _POSIX_C_SOURCE < 200112L
#undef  _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#endif

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <pthread.h>

#include "printerror.h"
#include "DBGpthread.h"

#define NUMTHREADS 2

/* dati da proteggere */
int thread1turn = 1;
int thread2turn = 0;
unsigned int thread1count = 0;
unsigned int thread2count = 0;

/* variabili per la sincronizzazione */
pthread_mutex_t mutex;
pthread_cond_t cond;

void *thread1(void *arg)
{
	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, "thread1");

		/* INIZIO SEZIONE CRITICA */
		/*verifica se è il turno del thread corrente */
		if (!thread1turn) {
			/* non è il turno del thread corrente, quindi il thread corrente aspetta */
			DBGpthread_cond_wait(&cond, &mutex, "thread1");
		}
		/* è il turno del thread corrente */
		/* imposta il prossimo turno in modo che non sia del thread corrente */
		thread1turn = 0;
		/* imposta il prossimo turno in modo che sia del prossimo thread */
		thread2turn = 1;
		/* verifica sincronizzazione */
		thread1count++;
		if (thread1count != (thread2count + 1)) {
			printf("Il thread1 thread ha riscontrato una violazione di sincronizzazione \n");
			exit(1);
		}
		printf("Il thread1 esegue la sua azione \n");
		fflush(stdout);
		sleep(1);
		/* segnala al prossimo thread che sarà il suo turno */
		DBGpthread_cond_signal(&cond, "thread1");
		/* FINE SEZIONE CRITICA */

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, "thread1");
	}
	pthread_exit(NULL);
}

void *thread2(void *arg)
{
	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, "thread2");

		/* INIZIO SEZIONE CRITICA*/
		/*verifica se è il turno del thread corrente */
		if (!thread2turn) {
			/* non è il turno del thread corrente, quindi il thread corrente aspetta */
			DBGpthread_cond_wait(&cond, &mutex, "thread2");
		}
		/* è il turno del thread corrente */
		/* imposta il prossimo turno in modo che non sia del thread corrente */
		thread2turn = 0;
		/* imposta il prossimo turno in modo che sia del prossimo thread */
		thread1turn = 1;
		/* verifica sincronizzazione */
		thread2count++;
		if (thread1count != thread2count) {
			printf("Il thread2 thread ha riscontrato una violazione di sincronizzazione \n");
			exit(1);
		}
		printf("Il thread2 esegue la sua azione \n");
		fflush(stdout);
		sleep(1);
		/* segnala al prossimo thread che sarà il suo turno */
		DBGpthread_cond_signal(&cond, "thread2");
		/* FINE SEZIONE CRITICA */

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, "thread2");
	}
	pthread_exit(NULL);
}

int main(void)
{
	pthread_t tid[NUMTHREADS];
	int  rc;
	intptr_t i;
	void *ptr;

	/* inizializzazione mutex */
	DBGpthread_mutex_init(&mutex, NULL, "mutex_init failed");

	/* inizializzazione cond */
	DBGpthread_cond_init(&cond, NULL, "cond_init failed");

	/* imposta la condizione iniziale */
	thread1turn = 1;
	thread2turn = 0;

	/* crea thread1 */
	rc = pthread_create(&(tid[0]), NULL, thread1, NULL);
	if (rc) PrintERROR_andExit(rc, "pthread_create failed");

	/* crea thread2 */
	rc = pthread_create(&(tid[1]), NULL, thread2, NULL);
	if (rc) PrintERROR_andExit(rc, "pthread_create failed");

	/* i thread sono in loop infinito, questo che segue non dovrebbe servire */
	/* join dei thread creati */
	for (i = 0; i < NUMTHREADS; i++) {
		rc = pthread_join(tid[i], &ptr);
		if (rc) PrintERROR_andExit(rc, "pthread_join failed");
	}

	/* distruzione mutex */
	rc = pthread_mutex_destroy(&mutex);
	if (rc) PrintERROR_andExit(rc, "pthread_mutex_destroy failed");

	/* distruzione cond */
	rc = pthread_cond_destroy(&cond);
	if (rc) PrintERROR_andExit(rc, "pthread_cond_destroy failed");

	pthread_exit(NULL);
	return(0);
}
