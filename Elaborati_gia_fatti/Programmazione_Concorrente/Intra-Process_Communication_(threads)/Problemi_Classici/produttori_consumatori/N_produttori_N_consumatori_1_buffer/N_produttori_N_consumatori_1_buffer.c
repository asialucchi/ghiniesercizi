/* N Produttori e N Consumatori che si scambiano prodotti mediante un unico Buffer */

#ifndef _thread_SAFE
#define _thread_SAFE
#endif
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#else
#if _POSIX_C_SOURCE < 200112L
#undef  _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <pthread.h> 
#include <inttypes.h>

#include "printerror.h"
#include "DBGpthread.h"

#define NUMPROD 5
#define NUMCONS 3
#define NUMBUFFER 1

/* dati da proteggere */
int buffer = 0;
int numFilledBuffer = 0;
int numProducerWaiting = 0;
int numProducerWaitingAndSignalled = 0;
int numConsumerWaiting = 0;
int numConsumerWaitingAndSignalled = 0;

/* variabili per la sincronizzazione */
pthread_mutex_t mutex;
pthread_cond_t condProducer;
pthread_cond_t condConsumer;

void *producer(void *arg)
{
	intptr_t index = (intptr_t)arg;
	int producedItem = 0;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread producer %lu"PRIiPTR, (unsigned long int) index);

	while (1) {
		/* avviene la produzione */
		producedItem++;

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		/* INIZIO SEZIONE CRITICA */
		/* verifica se deve mettersi in attesa */
		if (numProducerWaitingAndSignalled >= (NUMBUFFER - numFilledBuffer)) {
			printf("Il thread producer %"PRIiPTR" entra in attesa \n", index);
			fflush(stdout);
			sleep(1);
			numProducerWaiting++;
			/* non è il turno del thread corrente, quindi il thread corrente aspetta */
			DBGpthread_cond_wait(&condProducer, &mutex, label);
			numProducerWaiting--;
			numProducerWaitingAndSignalled--;
		}
		/* è il turno del thread corrente */
		else {
			printf("Il thread producer %"PRIiPTR" salta l'attesa \n", index);
		}
		/* verifica di coerenza dello stato del buffer */
		if (numFilledBuffer != 0) {
			printf("Il thread producer %"PRIiPTR" rileva che il buffer è pieno \n", index);
			exit(1);
		}
		/* riempie il buffer col dato prodotto */
		buffer = producedItem;
		/* aggiorna lo stato dei buffer */
		numFilledBuffer++;
		printf("Il thread producer %"PRIiPTR" ha riempito il buffer con il dato \"%d\" \n", index, buffer);
		fflush(stdout);
		sleep(1);
		/* verifica di coerenza dello stato del buffer */
		if (numFilledBuffer == 0) {
			printf("Il thread producer %"PRIiPTR" rileva che il buffer è vuoto \n", index);
			exit(1);
		}
		/* verifica se ci sono thread consumatori in attesa */
		if (
			(numConsumerWaitingAndSignalled < numConsumerWaiting)
			&&
			(numConsumerWaitingAndSignalled < numFilledBuffer)
			) {
			/* segnala a un thread consumer che è il suo turno */
			DBGpthread_cond_signal(&condConsumer, label);
			numConsumerWaitingAndSignalled++;
		}
		/* FINE SEZIONE CRITICA */

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);
	}
	pthread_exit(NULL);
}

void *consumer(void *arg)
{
	intptr_t index = (intptr_t)arg;
	int consumedItem;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread consumer %lu"PRIiPTR, (unsigned long int) index);


	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		/* INIZIO SEZIONE CRITICA */
		/* verifica se deve mettersi in attesa */
		if (numConsumerWaitingAndSignalled >= numFilledBuffer) {
			printf("Il thread consumer %"PRIiPTR" entra in attesa \n", index);
			fflush(stdout);
			sleep(1);
			numConsumerWaiting++;
			/* non è il turno del thread corrente, quindi il thread corrente aspetta */
			DBGpthread_cond_wait(&condConsumer, &mutex, label);
			numConsumerWaiting--;
			numConsumerWaitingAndSignalled--;
		}
		/* è il turno del thread corrente */
		else {
			printf("Il thread consumer %"PRIiPTR" salta l'attesa \n", index);
		}
		/* verifica di coerenza dello stato del buffer */
		if (numFilledBuffer == 0) {
			printf("Il thread consumer %"PRIiPTR" rileva che il buffer è vuoto \n", index);
			exit(1);
		}
		/* prende ciò che c'è nel buffer */
		consumedItem = buffer;
		/* aggiorna lo stato dei buffer */
		numFilledBuffer--;
		printf("Il thread consumer %"PRIiPTR" ha preso dal buffer il dato \"%d\" \n", index, consumedItem);
		fflush(stdout);
		sleep(1);
		/* verifica di coerenza dello stato del buffer */
		if (numFilledBuffer != 0) {
			printf("Il thread producer %"PRIiPTR" rileva che il buffer è pieno \n", index);
			exit(1);
		}
		/* verifica se ci sono thread produttori in attesa */
		if (numProducerWaitingAndSignalled < numProducerWaiting
				&& numProducerWaitingAndSignalled < (NUMBUFFER - numFilledBuffer)) {
			/* segnala a un thread producer che è il suo turno */
			DBGpthread_cond_signal(&condProducer, label);
			numProducerWaitingAndSignalled++;
		}
		/* FINE SEZIONE CRITICA */

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);
	}
	pthread_exit(NULL);
}

int main(void)
{
	pthread_t tid;
	int rc;
	intptr_t i;

	/* inizializzazione mutex */
	DBGpthread_mutex_init(&mutex, NULL, "mutex_init");

	/* inizializzazione cond */
	DBGpthread_cond_init(&condProducer, NULL, "cond_init");
	DBGpthread_cond_init(&condConsumer, NULL, "cond_init");

	/* imposta le condizioni iniziali */
	numFilledBuffer = 0;
	numProducerWaiting = 0;
	numProducerWaitingAndSignalled = 0;
	numConsumerWaiting = 0;
	numConsumerWaitingAndSignalled = 0;

	/* crea i thread */
	for (i = 0; i < NUMPROD; i++) {
		rc = pthread_create(&tid, NULL, producer, (void*)i);
		if (rc) PrintERROR_andExit(rc, "pthread_create failed");
	}

	for (i = 0; i < NUMCONS; i++) {
		rc = pthread_create(&tid, NULL, consumer, (void*)i);
		if (rc) PrintERROR_andExit(rc, "pthread_create failed");
	}

	/* i thread sono in loop infinito */
	pthread_exit(NULL);
	return(0);
}
