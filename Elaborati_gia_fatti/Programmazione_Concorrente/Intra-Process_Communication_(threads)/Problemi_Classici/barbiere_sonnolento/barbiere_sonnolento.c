/* Problema del barbiere sonnolento */

#ifndef _thread_SAFE
#define _thread_SAFE
#endif
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#else
#if _POSIX_C_SOURCE < 200112L
#undef  _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <pthread.h> 
#include <inttypes.h>

#include "printerror.h"
#include "DBGpthread.h"

#define NUMCHAIRS 5
#define NUMCUSTOMERS 50

/* dati da proteggere */
int numWaitingClient = 0;

/* variabili per la sincronizzazione */
pthread_mutex_t mutex;
pthread_cond_t condBarberReady;
pthread_cond_t condCustomerReady;
pthread_cond_t condMessage;

void *barber(void *arg)
{
	intptr_t index = (intptr_t)arg;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread barber %lu"PRIiPTR, (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		/* INIZIO SEZIONE CRITICA */
		/*verifica se è il turno del thread corrente */
		if (numWaitingClient <= 0) {
			/* non è il turno del thread corrente, quindi il thread corrente aspetta */
			DBGpthread_cond_wait(&condCustomerReady, &mutex, label);
		}
		/* è il turno del thread corrente */
		/* segnala a un thread cutomer che è il suo turno */
		DBGpthread_cond_signal(&condBarberReady, label);
		printf("Il thread barber %" PRIiPTR " ha segnalato a un thread customer che è il suo turno \n", index);
		fflush(stdout);
		/* attende conferma dal thread customer */
		DBGpthread_cond_wait(&condMessage, &mutex, label);
		printf("Il thread barber %" PRIiPTR " ha avuto conferma dal thread customer di aver ricevuto il turno \n", index);
		fflush(stdout);
		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);
		printf("Il thread barber %" PRIiPTR " esegue la sua azione \n", index);
		fflush(stdout);
		sleep(1);
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);
		/* segnala al un thread cutomer che non è più il suo turno */
		DBGpthread_cond_signal(&condMessage, label);
		printf("Il thread barber %" PRIiPTR " ha segnalato al thread customer che non è più il suo turno \n", index);
		fflush(stdout);
		/* attende conferma dal thread customer */
		DBGpthread_cond_wait(&condMessage, &mutex, label);
		printf("Il thread barber %" PRIiPTR " ha avuto conferma dal thread customer di aver terminato il turno \n", index);
		fflush(stdout);
		/* FINE SEZIONE CRITICA */

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);
	}
	pthread_exit(NULL);
}

void *customer(void *arg)
{
	intptr_t index = (intptr_t)arg;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread customer %lu"PRIiPTR, (unsigned long int) index);

	while (1) {
		/* simula il tempo di ritorno del customer dal barber */
		sleep(1);

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		/* INIZIO SEZIONE CRITICA */
		/*verifica se è il turno del thread corrente */
		if (numWaitingClient >= NUMCHAIRS) {
			/* rilascia la mutua esclusione */
			DBGpthread_mutex_unlock(&mutex, label);
		}
		else {
			numWaitingClient++;
			if (numWaitingClient <= 1) {
				/* segnala al thread barber che è il suo turno */
				DBGpthread_cond_signal(&condCustomerReady, label);
			}
			/* attende conferma dal thread barber dell'ottenimento del proprio turno */
			DBGpthread_cond_wait(&condBarberReady, &mutex, label);
			printf("Il thread customer %" PRIiPTR " ha ricevuto conferma dal thread barber che è il suo turno \n", index);
			fflush(stdout);
			/* è il turno del thread corrente */
			numWaitingClient--;
			/* segnala al thread barber di aver ricevuto il turno */
			DBGpthread_cond_signal(&condMessage, label);
			printf("Il thread customer %" PRIiPTR " ha inviato conferma al thread barber di aver ricevuto il turno \n", index);
			fflush(stdout);
			/* attende conferma dal thread barber di aver terminato il proprio turno */
			DBGpthread_cond_wait(&condMessage, &mutex, label);
			printf("Il thread customer %" PRIiPTR " ha ricevuto conferma dal thread barber che non è più il suo turno \n", index);
			fflush(stdout);
			/* segnala al thread barber di aver terminato il turno */
			DBGpthread_cond_signal(&condMessage, label);
			printf("Il thread customer %" PRIiPTR " ha inviato conferma al thread barber di aver terminato il turno \n", index);
			fflush(stdout);
			/* FINE SEZIONE CRITICA */

			/* rilascia la mutua esclusione */
			DBGpthread_mutex_unlock(&mutex, label);
		}
	}
	pthread_exit(NULL);
}

int main(void)
{
	pthread_t tid;
	int  rc;
	intptr_t i;

	/* inizializzazione mutex */
	DBGpthread_mutex_init(&mutex, NULL, "mutex_init");

	/* inizializzazione cond */
	DBGpthread_cond_init(&condBarberReady, NULL, "condBarberReady");
	DBGpthread_cond_init(&condCustomerReady, NULL, "condCustomerReady");
	DBGpthread_cond_init(&condMessage, NULL, "condMessage");

	/* imposta le condizioni iniziali */
	numWaitingClient = 0;

	/* crea i thread */
	i = 0;
	rc = pthread_create(&tid, NULL, barber, (void*)i);
	if (rc) PrintERROR_andExit(rc, "pthread_create failed");
	for (i = 0; i < NUMCUSTOMERS; i++) {
		rc = pthread_create(&tid, NULL, customer, (void*)i);
		if (rc) PrintERROR_andExit(rc, "pthread_create failed");
	}

	/* i thread sono in loop infinito */
	pthread_exit(NULL);
	return(0);
}
