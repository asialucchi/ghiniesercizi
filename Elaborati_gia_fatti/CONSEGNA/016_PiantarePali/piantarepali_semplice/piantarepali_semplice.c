/* piantarepali */

#ifndef _thread_SAFE
#define _thread_SAFE
#endif
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#else
#if _POSIX_C_SOURCE < 200112L
#undef  _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <pthread.h> 
#include <inttypes.h>

#include "printerror.h"
#include "DBGpthread.h"

#define NUMPOLES 2
#define NUMHAMMER 2

#define TODO 0
#define DONE 1

#define NOTGIVEN 0
#define GIVEN 1

/* dati da proteggere */
int numHammerDoing = 0;
int numHammerDone = 0;
int hammerStatus[NUMHAMMER];
int commandHammerStart = 0;

/* variabili per la sincronizzazione */
pthread_mutex_t mutex;
pthread_cond_t condHammerStart;
pthread_cond_t condHammerFinish;


void *poleHolder(void *arg)
{
	intptr_t index = (intptr_t)arg;
	int i;
	int loopCounter = 0;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread poleHolder %lu"PRIiPTR, (unsigned long int) index);

	while (1) {
		printf("Pali presi\n");
		sleep(1);

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		numHammerDoing = 0;
		numHammerDone = 0;
		for (i = 0; i < NUMHAMMER; ++i) {
			hammerStatus[i] = TODO;
		}

		printf("Martellare!\n");
		fflush(stdout);
		commandHammerStart = GIVEN;
		DBGpthread_cond_broadcast(&condHammerStart, label);

		DBGpthread_cond_wait(&condHammerFinish, &mutex, label);

		loopCounter++;

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);

		if (loopCounter % 5 == 0) {
			printf("Reggipali%"PRIiPTR" in pausa\n", index);
			fflush(stdout);
			sleep(3);
		}
	}
}

void *hammer(void *arg)
{
	intptr_t index = (intptr_t)arg;
	int loopCounter = 0;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread hammer %lu"PRIiPTR, (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		while (commandHammerStart == NOTGIVEN || hammerStatus[index] == DONE) {
			DBGpthread_cond_wait(&condHammerStart, &mutex, label);
		}

		printf("Martello%"PRIiPTR" inizia\n", index);
		fflush(stdout);
		numHammerDoing++;
		if (numHammerDoing == NUMPOLES) {
			commandHammerStart = NOTGIVEN;
		}

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);

		sleep(1);

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		hammerStatus[index] = DONE;
		numHammerDone++;
		printf("Martello%"PRIiPTR" finisce\n", index);
		fflush(stdout);
		if (numHammerDone == NUMPOLES) {
			DBGpthread_cond_signal(&condHammerFinish, label);
		}

		loopCounter++;

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);

		if (loopCounter % 4 == 0) {
			printf("Martello%"PRIiPTR" in pausa\n", index);
			fflush(stdout);
			if (index == 0) {
				sleep(5);
			}
			else if (index == 1) {
				sleep(7);
			}
		}
	}
}

int main(void)
{
	pthread_t tid;
	int  rc;
	intptr_t i;

	/* inizializzazione mutex */
	DBGpthread_mutex_init(&mutex, NULL, "mutex_init");

	/* inizializzazione cond */
	DBGpthread_cond_init(&condHammerStart, NULL, "cond");
	DBGpthread_cond_init(&condHammerFinish, NULL, "cond");

	/* imposta le condizioni iniziali */
	numHammerDoing = 0;
	numHammerDone = 0;
	for (i = 0; i < NUMHAMMER; ++i) {
		hammerStatus[i] = TODO;
	}
	commandHammerStart = NOTGIVEN;

	/* crea i thread */
	i = 0;
	rc = pthread_create(&tid, NULL, poleHolder, (void*)i);
	if (rc) PrintERROR_andExit(rc, "pthread_create failed");

	for (i = 0; i < NUMHAMMER; i++) {
		rc = pthread_create(&tid, NULL, hammer, (void*)i);
		if (rc) PrintERROR_andExit(rc, "pthread_create failed");
	}

	/* i thread sono in loop infinito */
	pthread_exit(NULL);
	return(0);
}
