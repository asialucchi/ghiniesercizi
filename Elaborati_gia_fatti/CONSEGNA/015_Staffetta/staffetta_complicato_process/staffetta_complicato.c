/* staffetta */

#ifndef _thread_SAFE
#define _thread_SAFE
#endif
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#else
#if _POSIX_C_SOURCE < 200112L
#undef  _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <pthread.h> 
#include <inttypes.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

#include "printerror.h"
#include "DBGpthread.h"

#define SHARED_MEMORY_OBJECT_PATH "/smo" /* normalmente /dev/shm */

#define ALFASIZE 5
#define BETASIZE 4

#define ALFA 0
#define BETA 1

#define OCCUPIED 0
#define VACANT 1

/* struttura dello shared memory object */
typedef struct {
	/* dati da proteggere */
	int trakStatus;
	int groupTurn;
	int numAlfaLap[ALFASIZE];
	int numBetaLap[BETASIZE];

	/* variabili per la sincronizzazione */
	pthread_mutex_t mutex;
	pthread_cond_t condTurnAlfa;
	pthread_cond_t condTurnBeta;
} shmo_t;

int summation(int *array, int size) {
	int i = 0;
	int result = 0;
	for (i = 0; array != NULL && size > 0 && i < size; ++i)
	{
		result = result + array[i];
	}
	return result;
}

void *alfa(shmo_t *shmo, int index)
{
	int i = 0;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread alfa %lu", (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&(shmo->mutex), label);

		while (shmo->groupTurn != ALFA || shmo->numAlfaLap[index] != 0 || shmo->trakStatus == OCCUPIED) {
			DBGpthread_cond_wait(&(shmo->condTurnAlfa), &(shmo->mutex), label);
		}
		shmo->trakStatus = OCCUPIED;

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&(shmo->mutex), label);

		printf("Alfa%d inizia\n", index);
		fflush(stdout);
		sleep(1);
		printf("Alfa%d finisce\n", index);
		fflush(stdout);

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&(shmo->mutex), label);

		shmo->trakStatus = VACANT;
		(shmo->numAlfaLap)[index]++;
		shmo->groupTurn = BETA;

		if (summation(shmo->numAlfaLap, ALFASIZE) == ALFASIZE)
		{
			for (i = 0; i < ALFASIZE; ++i)
			{
				(shmo->numAlfaLap)[i] = 0;
			}
		}

		DBGpthread_cond_signal(&(shmo->condTurnBeta), label);

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&(shmo->mutex), label);
	}
}

void *beta(shmo_t *shmo, int index)
{
	int i = 0;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread beta %lu", (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&(shmo->mutex), label);

		while (shmo->groupTurn != BETA || shmo->numBetaLap[index] != 0 || shmo->trakStatus == OCCUPIED) {
			DBGpthread_cond_wait(&(shmo->condTurnBeta), &(shmo->mutex), label);
		}
		shmo->trakStatus = OCCUPIED;

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&(shmo->mutex), label);

		printf("Beta%d inizia\n", index);
		fflush(stdout);
		sleep(1);
		printf("Beta%d finisce\n", index);
		fflush(stdout);

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&(shmo->mutex), label);

		shmo->trakStatus = VACANT;
		shmo->numBetaLap[index]++;
		shmo->groupTurn = ALFA;

		if (summation(shmo->numBetaLap, BETASIZE) == BETASIZE)
		{
			for (i = 0; i < BETASIZE; ++i)
			{
				(shmo->numBetaLap)[i] = 0;
			}
		}

		DBGpthread_cond_signal(&(shmo->condTurnAlfa), label);

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&(shmo->mutex), label);
	}
}

void handle_signal(int signum) {
	if (shm_unlink(SHARED_MEMORY_OBJECT_PATH) != 0) {
		perror("shm_unlink failed");
		kill(0, SIGTERM);
		exit(1);
	}
	kill(0, SIGTERM);
}

void register_signal_handler()
{
	struct sigaction sa;
	sa.sa_handler = &handle_signal;
	sigemptyset(&sa.sa_mask);
	if (sigaction(SIGINT, &sa, 0) == -1) {
		PrintERROR_andExit(errno, "sigaction failed ");
	}
}

int main(void)
{
	int shmfd;
	int rc;
	pid_t pid;
	shmo_t *shmo;
	int shmo_size = sizeof(shmo_t);
	pthread_mutexattr_t mattr;
	pthread_condattr_t cvattr;
	int i;

	/* crea uno shared memory object */
	shmfd = shm_open(SHARED_MEMORY_OBJECT_PATH, O_CREAT /*| O_EXCL*/ | O_RDWR, S_IRWXU);
	if (shmfd < 0) {
		perror("shm_open failed");
		exit(1);
	}
	/* imposta la dimensione dello shared memory object */
	rc = ftruncate(shmfd, shmo_size);
	if (rc != 0) {
		perror("ftruncate failed");
		exit(1);
	}

	/* crea un puntatore allo shared memory object */
	shmo = (shmo_t *)mmap(NULL, sizeof(shmo_t), PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0);
	if (shmo == MAP_FAILED) {
		perror("mmap failed");
		exit(1);
	}

	/* imposta le condizioni iniziali */
	shmo->groupTurn = ALFA;
	for (i = 0; i < ALFASIZE; ++i)
	{
		(shmo->numAlfaLap)[i] = 0;
	}
	for (i = 0; i < BETASIZE; ++i)
	{
		(shmo->numBetaLap)[i] = 0;
	}
	shmo->trakStatus = VACANT;

	/* inizializzazione mutexattr */
	rc = pthread_mutexattr_init(&mattr);
	if (rc) PrintERROR_andExit(errno, "pthread_mutexattr_init failed");
	rc = pthread_mutexattr_setpshared(&mattr, PTHREAD_PROCESS_SHARED);
	if (rc) PrintERROR_andExit(errno, "pthread_mutexattr_setpshared failed");

	/* inizializzazione mutex */
	rc = pthread_mutex_init(&shmo->mutex, &mattr);
	if (rc) PrintERROR_andExit(errno, "pthread_mutex_init failed");

	/* inizializzazione condxattr */
	rc = pthread_condattr_init(&cvattr);
	if (rc) PrintERROR_andExit(errno, "pthread_condattr_init failed");
	rc = pthread_condattr_setpshared(&cvattr, PTHREAD_PROCESS_SHARED);
	if (rc) PrintERROR_andExit(errno, "pthread_condattr_setpshared failed");

	/* inizializzazione cond */
	rc = pthread_cond_init(&(shmo->condTurnAlfa), &cvattr);
	if (rc) PrintERROR_andExit(errno, "pthread_cond_init failed");
	rc = pthread_cond_init(&(shmo->condTurnBeta), &cvattr);
	if (rc) PrintERROR_andExit(errno, "pthread_cond_init failed");


	/* genera processi figlio */
	for (i = 0; i < ALFASIZE; i++) {
		pid = fork();
		if (pid < 0) {
			PrintERROR_andExit(errno, "fork failed");
		}
		else if (pid == 0) {
			/* figlio */
			alfa(shmo, i);
			exit(0);
		}
	}

	for (i = 0; i < BETASIZE; i++) {
		pid = fork();
		if (pid < 0) {
			PrintERROR_andExit(errno, "fork failed");
		}
		else if (pid == 0) {
			/* figlio */
			beta(shmo, i);
			exit(0);
		}
	}

	/* padre */
	register_signal_handler();
	while (1) {
		sleep(10);
	};

	return(0);
}
