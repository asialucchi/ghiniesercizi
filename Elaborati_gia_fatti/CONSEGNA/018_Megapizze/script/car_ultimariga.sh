#!/bin/bash

for FILENAME in $(find /usr/include/linux/ -maxdepth 1 -name "*f*.h" -print); do
	NUMROW=$(wc -l ${FILENAME})
	NUMROW=${NUMROW%% *}
	if (( "${NUMROW}">="10" && "${NUMROW}"<="100" )); then
		echo $(tail -n 1 ${FILENAME} | wc -m)
	fi
done

exit 0