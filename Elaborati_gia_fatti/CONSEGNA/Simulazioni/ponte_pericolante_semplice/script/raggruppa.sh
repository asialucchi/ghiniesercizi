#!/bin/bash

TMP=tmp.txt
if [[ -e ${TMP} ]]; then
	rm -f ${TMP}
fi

FILENAME=cadutevic.txt
exec {FD}<./${FILENAME}
while read -u ${FD} DATA LUOGO MOTIVO DANNI; do
	echo "${MOTIVO} $(grep ${MOTIVO} ${FILENAME} | wc -l)" >> ${TMP}
done
exec {FD}>&-

# ordina, filtra ed emette sullo stdout motivo e numero delle cadute
sort ${TMP} | uniq
rm -f ${TMP}

exit 0
