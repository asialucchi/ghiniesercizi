#!/bin/bash

FILENAME=cadutevic2.txt
exec {FD}<./${FILENAME}
while read -u ${FD} ROW; do
	# pattern " viene sostituito con niente
	TMP=${ROW#\"*\"*\"*\"*\"}
	echo "${TMP%\"*\"*\"}"
done
exec {FD}>&-

exit 0
