#!/bin/bash

TMP=tmp.txt
if [[ -e ${TMP} ]]; then
	rm -f ${TMP}
fi

# per ogni nome di file nella directory /usr/include/ con estensione .h
for FILENAME in $(ls /usr/include/*.h); do
	# conta le righe che contengono ifdef e se sono 10 o più
	if (( "$(cat ${FILENAME} | grep ifdef | wc -l)">="10" )); then
		# le scrive su un file
		cat ${FILENAME} | grep ifdef | head -n 5 >> ${TMP}
	fi
done

sort ${TMP} -o FINALE.txt
rm -f ${TMP}

exit 0