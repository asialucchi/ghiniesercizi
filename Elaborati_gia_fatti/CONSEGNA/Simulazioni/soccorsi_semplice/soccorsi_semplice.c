/* Problema del barbiere sonnolento */

#ifndef _thread_SAFE
#define _thread_SAFE
#endif
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#else
#if _POSIX_C_SOURCE < 200112L
#undef  _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <pthread.h> 
#include <inttypes.h>

#include "printerror.h"
#include "DBGpthread.h"

#define TEACHING 0
#define SWEARING 1
#define HEALED 2

#define WAITING 0
#define ARRIVED 1
#define LEAVING 2

/* dati da proteggere */
int professorStatus = 0;
int medicStatus = 0;
int exorcistStatus = 0;

/* variabili per la sincronizzazione */
pthread_mutex_t mutex;
pthread_cond_t condSwearing;
pthread_cond_t condHealed;
pthread_cond_t condMedicArrived;
pthread_cond_t condExorcistArrived;
pthread_cond_t condMedicLeaving;
pthread_cond_t condExorcistLeaving;

void *professor(void *arg)
{
	intptr_t index = (intptr_t)arg;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread professor %lu"PRIiPTR, (unsigned long int) index);

	while (1) {
		printf("Il professore inizia la lezione \n");
		fflush(stdout);
		
		sleep(4);

		printf("Il professore è caduto \n");
		fflush(stdout);
		sleep(1);

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);
		professorStatus = SWEARING;

		printf("Il professore bestemmia gli dei \n");
		fflush(stdout);
		sleep(1);

		DBGpthread_cond_broadcast(&condSwearing, label);
		if (professorStatus != HEALED) {
			DBGpthread_cond_wait(&condHealed, &mutex, label);
		}

		/*printf("Il professore è stato soccorso \n");
		fflush(stdout);
		sleep(1);*/

		professorStatus = TEACHING;

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);
	}

}

void *medic(void *arg)
{
	intptr_t index = (intptr_t)arg;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread medic %lu"PRIiPTR, (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);
		
		medicStatus = WAITING;
		if (professorStatus != SWEARING) {
			DBGpthread_cond_wait(&condSwearing, &mutex, label);
		}

		medicStatus = ARRIVED;
		DBGpthread_cond_signal(&condMedicArrived, label);
		printf("Il medico è arrivato \n");
		fflush(stdout);
		sleep(1);
		if (exorcistStatus != ARRIVED) {
			DBGpthread_cond_wait(&condExorcistArrived, &mutex, label);
		}

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);

		sleep(2);

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		if (professorStatus != HEALED) {
			professorStatus = HEALED;
			printf("Il professore è stato soccorso \n");
			fflush(stdout);
			sleep(1);
			DBGpthread_cond_signal(&condHealed, label);
		}

		medicStatus = LEAVING;
		DBGpthread_cond_signal(&condMedicLeaving, label);
		if (exorcistStatus != LEAVING) {
			DBGpthread_cond_wait(&condExorcistLeaving, &mutex, label);
		}
		printf("Il medico se ne va \n");
		fflush(stdout);
		sleep(1);

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);
	}

}

void *exorcist(void *arg)
{
	intptr_t index = (intptr_t)arg;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread exorcist %lu"PRIiPTR, (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);
		
		exorcistStatus = WAITING;
		if (professorStatus != SWEARING) {
			DBGpthread_cond_wait(&condSwearing, &mutex, label);
		}

		exorcistStatus = ARRIVED;
		DBGpthread_cond_signal(&condExorcistArrived, label);
		printf("L\'esorcista è arrivato \n");
		fflush(stdout);
		sleep(1);
		if (medicStatus != ARRIVED) {
			DBGpthread_cond_wait(&condMedicArrived, &mutex, label);
		}

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);

		sleep(2);

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		if (professorStatus != HEALED) {
			professorStatus = HEALED;
			printf("Il professore è stato soccorso \n");
			fflush(stdout);
			sleep(1);
			DBGpthread_cond_signal(&condHealed, label);
		}

		exorcistStatus = LEAVING;
		DBGpthread_cond_signal(&condExorcistLeaving, label);
		if (medicStatus != LEAVING) {
			DBGpthread_cond_wait(&condMedicLeaving, &mutex, label);
		}
		printf("L\'esorcista se ne va \n");
		fflush(stdout);
		sleep(1);

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);
	}

}

int main(void)
{
	pthread_t tid;
	int  rc;
	intptr_t i;

	/* inizializzazione mutex */
	DBGpthread_mutex_init(&mutex, NULL, "mutex_init");

	/* inizializzazione cond */
	DBGpthread_cond_init(&condSwearing, NULL, "cond");
	DBGpthread_cond_init(&condHealed, NULL, "cond");
	DBGpthread_cond_init(&condMedicArrived, NULL, "cond");
	DBGpthread_cond_init(&condExorcistArrived, NULL, "cond");
	DBGpthread_cond_init(&condMedicLeaving, NULL, "cond");
	DBGpthread_cond_init(&condExorcistLeaving, NULL, "cond");

	/* imposta le condizioni iniziali */
	professorStatus = TEACHING;
	medicStatus = WAITING;
	exorcistStatus = WAITING;
	

	/* crea i thread */
	i = 0;
	rc = pthread_create(&tid, NULL, professor, (void*)i);
	if (rc) PrintERROR_andExit(rc, "pthread_create failed");

	rc = pthread_create(&tid, NULL, medic, (void*)i);
	if (rc) PrintERROR_andExit(rc, "pthread_create failed");

	rc = pthread_create(&tid, NULL, exorcist, (void*)i);
	if (rc) PrintERROR_andExit(rc, "pthread_create failed");

	/* i thread sono in loop infinito */
	pthread_exit(NULL);
	return(0);
}
