#!/bin/bash

if (( "$#"!="2" )) ; then
	echo "numero argomenti errato"
	exit 1
fi

EMAIL=$1
MATRICOLA=$2

RIGA=$(cat ./matricola.txt | grep ${MATRICOLA})

if [[ -z "${RIGA}" ]]; then
	echo "account non trovato"
	exit 1
fi

RIGA="${RIGA%	*	*}"
NOME="${RIGA%	*}"
COGNOME="${RIGA#*	}"

echo "la account ${EMAIL} appartiene allo studente ${NOME} ${COGNOME}"

exit 0
