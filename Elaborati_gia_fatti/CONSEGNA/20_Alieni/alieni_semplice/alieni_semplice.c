/* alieni */

#ifndef _thread_SAFE
#define _thread_SAFE
#endif
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#else
#if _POSIX_C_SOURCE < 200112L
#undef  _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <pthread.h> 
#include <inttypes.h>

#include "printerror.h"
#include "DBGpthread.h"

#define INITIALNUMWARNER 10

/* dati da proteggere */
int numWarner = 0;
pthread_t threadIdentifier;

/* variabili per la sincronizzazione */
pthread_mutex_t mutex;
pthread_cond_t condNewWarner;
pthread_cond_t condNextWarner;


void *warner(void *arg)
{
	pthread_t tid = pthread_self();
	int rc;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread warner %lu", tid);

	sleep(1);

	/* prende la mutua esclusione */
	DBGpthread_mutex_lock(&mutex, label);
	
	while (numWarner != 0) {
		DBGpthread_cond_wait(&condNextWarner, &mutex, label);
	}

	threadIdentifier = tid;
	DBGpthread_cond_signal(&condNewWarner, label);
	printf("thread warner %lu ha inviato tid\n", tid);
	fflush(stdout);
	
	numWarner++;

	/* rilascia la mutua esclusione */
	DBGpthread_mutex_unlock(&mutex, label);

	/* crea il thread */
	printf("thread warner %lu ha terminato\n", tid);
	fflush(stdout);

	rc = pthread_create(&tid, NULL, warner, (void*)NULL);
	if (rc) PrintERROR_andExit(rc, "pthread_create failed");
	
	pthread_exit(NULL);
}

int main(void)
{
	pthread_t tid;
	int  rc;
	int i;
	void *ptr;

	/* inizializzazione mutex */
	DBGpthread_mutex_init(&mutex, NULL, "mutex_init");

	/* inizializzazione cond */
	DBGpthread_cond_init(&condNewWarner, NULL, "cond");
	DBGpthread_cond_init(&condNextWarner, NULL, "cond");

	/* imposta le condizioni iniziali */
	numWarner = 0;

	/* crea i thread */
	for (i = 0; i < INITIALNUMWARNER; i++) {
		rc = pthread_create(&tid, NULL, warner, (void*)NULL);
		if (rc) PrintERROR_andExit(rc, "pthread_create failed");
	}

	while (1) {
		ptr = NULL;

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, "main");
		
		while (numWarner == 0) {
			DBGpthread_cond_wait(&condNewWarner, &mutex, "main");
		}

		tid = threadIdentifier;
		DBGpthread_cond_broadcast(&condNextWarner, "main");
		printf("main riceve tid %lu\n", tid);
		fflush(stdout);

		numWarner--;
		
		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, "main");

		/* join dei thread creati */
		rc = pthread_join(tid, &ptr);
		if (rc) PrintERROR_andExit(rc, "pthread_join failed");

		printf("main ha rilevato la terminazione del thread warner %lu\n", tid);
		fflush(stdout);
	}

	/* distruzione mutex */
	rc = pthread_mutex_destroy(&mutex);
	if (rc) PrintERROR_andExit(rc, "pthread_mutex_destroy failed");

	/* distruzione cond */
	rc = pthread_cond_destroy(&condNewWarner);
	if (rc) PrintERROR_andExit(rc, "pthread_cond_destroy failed");
	rc = pthread_cond_destroy(&condNextWarner);
	if (rc) PrintERROR_andExit(rc, "pthread_cond_destroy failed");

	pthread_exit(NULL);
	return(0);
}
