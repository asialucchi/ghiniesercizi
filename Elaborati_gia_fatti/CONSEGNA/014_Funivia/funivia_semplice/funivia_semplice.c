/* funivia */

#ifndef _thread_SAFE
#define _thread_SAFE
#endif
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#else
#if _POSIX_C_SOURCE < 200112L
#undef  _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <pthread.h> 
#include <inttypes.h>

#include "printerror.h"
#include "DBGpthread.h"

#define CABLEWAYLEAVING 0
#define CABLEWAYARRIVED 1

#define NUMSOBER 4
#define NUMSEAT 2

/* dati da proteggere */
int cableWayStatus = 0;
int occupiedSeat = 0;
int passengerIndex[NUMSOBER];

/* variabili per la sincronizzazione */
pthread_mutex_t mutex;
pthread_cond_t condCableWayLeaving;
pthread_cond_t condPassengerOnBoard;
pthread_cond_t condCableWayArrived;
pthread_cond_t condPassengerOffBoard;


void *cableWay(void *arg)
{
	intptr_t index = (intptr_t)arg;
	int i;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread cableWay %lu"PRIiPTR, (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		cableWayStatus = CABLEWAYLEAVING;
		DBGpthread_cond_broadcast(&condCableWayLeaving, label);

		if (occupiedSeat < NUMSEAT){
			DBGpthread_cond_wait(&condPassengerOnBoard, &mutex, label);
		} 
		
		for(i = 0; i < NUMSEAT; i++) {
			printf("Il passeggero %d è a bordo\n", passengerIndex[i]);
		}

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);

		sleep(1);
		printf("La funivia è in cima al campanile\n");
		sleep(1);

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		cableWayStatus = CABLEWAYARRIVED;
		DBGpthread_cond_broadcast(&condCableWayArrived, label);

		if (occupiedSeat != 0){
			DBGpthread_cond_wait(&condPassengerOffBoard, &mutex, label);
		}

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);
	}
}

void *passenger(void *arg)
{
	intptr_t index = (intptr_t)arg;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread passenger %lu"PRIiPTR, (unsigned long int) index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&mutex, label);

		while (cableWayStatus != CABLEWAYLEAVING || occupiedSeat == NUMSEAT) {
			DBGpthread_cond_wait(&condCableWayLeaving, &mutex, label);
		}

		passengerIndex[occupiedSeat] = (int)index;
		occupiedSeat++;
		if (occupiedSeat == NUMSEAT) {
			DBGpthread_cond_signal(&condPassengerOnBoard,label);
		}
		
		while (cableWayStatus != CABLEWAYARRIVED) {
			DBGpthread_cond_wait(&condCableWayArrived, &mutex, label);
		}

		occupiedSeat--;
		if (occupiedSeat == 0) {
			DBGpthread_cond_signal(&condPassengerOffBoard, label);
		}

		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&mutex, label);

		printf("Il passeggero %"PRIiPTR" grida di gioia\n", index);
	}
}

int main(void)
{
	pthread_t tid;
	int  rc;
	intptr_t i;

	/* inizializzazione mutex */
	DBGpthread_mutex_init(&mutex, NULL, "mutex_init");

	/* inizializzazione cond */
	DBGpthread_cond_init(&condCableWayLeaving, NULL, "cond");
	DBGpthread_cond_init(&condPassengerOnBoard, NULL, "cond");
	DBGpthread_cond_init(&condCableWayArrived, NULL, "cond");
	DBGpthread_cond_init(&condPassengerOffBoard, NULL, "cond");

	/* imposta le condizioni iniziali */
	cableWayStatus = CABLEWAYARRIVED;
	occupiedSeat = 0;
	
	/* crea i thread */
	i = 0;
	rc = pthread_create(&tid, NULL, cableWay, (void*)i);
	if (rc) PrintERROR_andExit(rc, "pthread_create failed");

	for(i = 0; i < NUMSOBER; i++) {
		rc = pthread_create(&tid, NULL, passenger, (void*)i);
		if (rc) PrintERROR_andExit(rc, "pthread_create failed");
	}

	/* i thread sono in loop infinito */
	pthread_exit(NULL);
	return(0);
}
