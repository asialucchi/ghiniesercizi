/* semaforo */

#ifndef _thread_SAFE
#define _thread_SAFE
#endif
#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#else
#if _POSIX_C_SOURCE < 200112L
#undef  _POSIX_C_SOURCE
#define _POSIX_C_SOURCE 200112L
#endif
#endif

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <pthread.h> 
#include <inttypes.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

#include "printerror.h"
#include "DBGpthread.h"

#define SHARED_MEMORY_OBJECT_PATH "/smo" /* normalmente /dev/shm */

#define NUMAUTOMOBILI 6

#define ROSSO 0
#define ARANCIONE 1
#define VERDE 2

#define OCCUPATO 0
#define LIBERO 1

/* struttura dello shared memory object */
typedef struct {
	/* dati da proteggere */
	int statoSemaforo;
	int statoPassaggio;
	int turno;
	int ultimoNumeroEmesso;
	int numAttraversano;

	/* variabili per la sincronizzazione */
	pthread_mutex_t mutex;
	pthread_cond_t condVerde;
	pthread_cond_t condPassa;
	pthread_cond_t condLibero;
} shmo_t;


void semaforo(shmo_t *shmo, int index)
{
	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread semaforo %d", index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&(shmo->mutex), label);
		shmo->statoSemaforo = ARANCIONE;
		printf("SEMAFORO ARANCIONE\n");
		fflush(stdout);
		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&(shmo->mutex), label);

		sleep(1);

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&(shmo->mutex), label);
		if (shmo->statoPassaggio == OCCUPATO)
		{
			DBGpthread_cond_wait(&(shmo->condLibero), &(shmo->mutex), label);
		}
		shmo->statoSemaforo = ROSSO;
		printf("SEMAFORO ROSSO\n");
		fflush(stdout);
		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&(shmo->mutex), label);

		sleep(3);

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&(shmo->mutex), label);
		shmo->statoSemaforo = VERDE;
		printf("SEMAFORO VERDE\n");
		fflush(stdout);
		DBGpthread_cond_broadcast(&(shmo->condVerde), label);
		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&(shmo->mutex), label);

		sleep(4);
	}
}

void automobile(shmo_t *shmo, int index)
{
	int mioNumero = 0;

	/* prepara stringa da passare a pthread_mutex_lock */
	char label[100];
	sprintf(label, "thread automobile %d", index);

	while (1) {
		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&(shmo->mutex), label);
		while (shmo->statoSemaforo != VERDE) {
			DBGpthread_cond_wait(&(shmo->condVerde), &(shmo->mutex), label);
		}
		mioNumero = shmo->ultimoNumeroEmesso;
		shmo->ultimoNumeroEmesso = (shmo->ultimoNumeroEmesso+1)%NUMAUTOMOBILI;
		shmo->statoPassaggio = OCCUPATO;
		shmo->numAttraversano++;
		printf("automobile%d con il numero %d parte\n", index, mioNumero);
		fflush(stdout);
		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&(shmo->mutex), label);

		sleep(2);/* 2 secondi per l'attraversamento */

		/* prende la mutua esclusione */
		DBGpthread_mutex_lock(&(shmo->mutex), label);
		while (mioNumero != shmo->turno) {
			DBGpthread_cond_wait(&(shmo->condPassa), &(shmo->mutex), label);
		}
		shmo->numAttraversano--;
		printf("automobile%d con il numero %d ha attraversato\n", index, mioNumero);
		fflush(stdout);
		if (shmo->numAttraversano == 0) {
			shmo->statoPassaggio = LIBERO;
		}
		shmo->turno = (shmo->turno+1)%NUMAUTOMOBILI;
		DBGpthread_cond_signal(&(shmo->condPassa),label);
		if (shmo->statoPassaggio == LIBERO) {
			DBGpthread_cond_signal(&(shmo->condLibero),label);
		}
		/* rilascia la mutua esclusione */
		DBGpthread_mutex_unlock(&(shmo->mutex), label);

		sleep(5+index);/* 5+index secondi per ripresentarsi al semaforo */
	}
}

void handle_signal(int signum) {
	if (shm_unlink(SHARED_MEMORY_OBJECT_PATH) != 0) {
		perror("shm_unlink failed");
		kill(0, SIGTERM);
		exit(1);
	}
	kill(0, SIGTERM);
}

void register_signal_handler()
{
	struct sigaction sa;
	sa.sa_handler = &handle_signal;
	sigemptyset(&sa.sa_mask);
	if (sigaction(SIGINT, &sa, 0) == -1) {
		PrintERROR_andExit(errno, "sigaction failed ");
	}
}

int main(void)
{
	int shmfd;
	int rc;
	pid_t pid;
	shmo_t *shmo;
	int shmo_size = sizeof(shmo_t);
	pthread_mutexattr_t mattr;
	pthread_condattr_t cvattr;
	int i;

	/* crea uno shared memory object */
	shmfd = shm_open(SHARED_MEMORY_OBJECT_PATH, O_CREAT /*| O_EXCL*/ | O_RDWR, S_IRWXU);
	if (shmfd < 0) {
		perror("shm_open failed");
		exit(1);
	}
	/* imposta la dimensione dello shared memory object */
	rc = ftruncate(shmfd, shmo_size);
	if (rc != 0) {
		perror("ftruncate failed");
		exit(1);
	}

	/* crea un puntatore allo shared memory object */
	shmo = (shmo_t *)mmap(NULL, sizeof(shmo_t), PROT_READ | PROT_WRITE, MAP_SHARED, shmfd, 0);
	if (shmo == MAP_FAILED) {
		perror("mmap failed");
		exit(1);
	}

	/* imposta le condizioni iniziali */
	shmo->statoSemaforo = VERDE;
	shmo->statoPassaggio = LIBERO;
	shmo->turno = 0;
	shmo->ultimoNumeroEmesso = 0;
	shmo->numAttraversano = 0;
	
	/* inizializzazione mutexattr */
	rc = pthread_mutexattr_init(&mattr);
	if (rc) PrintERROR_andExit(errno, "pthread_mutexattr_init failed");
	rc = pthread_mutexattr_setpshared(&mattr, PTHREAD_PROCESS_SHARED);
	if (rc) PrintERROR_andExit(errno, "pthread_mutexattr_setpshared failed");

	/* inizializzazione mutex */
	rc = pthread_mutex_init(&shmo->mutex, &mattr);
	if (rc) PrintERROR_andExit(errno, "pthread_mutex_init failed");

	/* inizializzazione condxattr */
	rc = pthread_condattr_init(&cvattr);
	if (rc) PrintERROR_andExit(errno, "pthread_condattr_init failed");
	rc = pthread_condattr_setpshared(&cvattr, PTHREAD_PROCESS_SHARED);
	if (rc) PrintERROR_andExit(errno, "pthread_condattr_setpshared failed");

	/* inizializzazione cond */
	rc = pthread_cond_init(&(shmo->condVerde), &cvattr);
	if (rc) PrintERROR_andExit(errno, "pthread_cond_init failed");
	rc = pthread_cond_init(&(shmo->condPassa), &cvattr);
	if (rc) PrintERROR_andExit(errno, "pthread_cond_init failed");
	rc = pthread_cond_init(&(shmo->condLibero), &cvattr);
	if (rc) PrintERROR_andExit(errno, "pthread_cond_init failed");


	/* genera processi figlio */
	for (i = 0; i < NUMAUTOMOBILI; i++) {
		pid = fork();
		if (pid < 0) {
			PrintERROR_andExit(errno, "fork failed");
		}
		else if (pid == 0) {
			/* figlio */
			automobile(shmo, i);
			exit(0);
		}
	}

	/* padre */
	register_signal_handler();
	semaforo(shmo, i);

	return(0);
}
