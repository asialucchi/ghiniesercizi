/* acrossProcessBoundary.c  
   4-17 Synchronization Across Process Boundaries
   a causa dell'uso di strerror_r, usato in printerror.h,
   compilare con -D_POSIX_C_SOURCE >= 200112L
*/

/* messo prima perche' contiene define _POSIX_C_SOURCE */
#include "printerror.h"
#include "DBGpthread.h"


#include <stdlib.h>
#include <unistd.h>   /* exit() etc */
#include <stdio.h>
#include <string.h>     /* per strerror_r  and  memset */
#include <sys/mman.h>  /* Memory MANagement: shm_* stuff, and mmap() */
#include <sys/types.h>
#include <signal.h>
#include <sys/time.h>	/* timeval{} for select() */
#include <time.h>	/* timespec{} for pselect() */
#include <limits.h>	/* for OPEN_MAX */
#include <errno.h>
#include <sys/wait.h>
#include <assert.h>
#include <pthread.h>
#include <semaphore.h>


/*
#include <sys/ipc.h>
#include <sys/shm.h>
*/
#include <fcntl.h>
#include <sys/stat.h>

typedef struct { 		/* buffer di scambio */
     int all_sober;
     int fun_travelling;
     int nPersone;
     pthread_mutex_t mutex; 
     pthread_cond_t cond_wait_for_entrance; 
     pthread_cond_t cond_wait_for_end_of_travel; /*inizializzare */
     } 
buffer_t; 



void ubriaco(buffer_t *b, int identifier)
{
	int id = identifier;
	while(1){
		DBGpthread_mutex_lock(&b->mutex, "Lock ubriaco");
		while(b->fun_travelling || b->nPersone != 0){
			DBGpthread_cond_wait(&b->cond_wait_for_end_of_travel, &b->mutex, "Wait for funivia to finish");
		}
		/*se esco dalla wait vuol dire che sicuramnete la funivia non sta viaggiando e le persone sono minori di 2*/
		printf("UBRIACO id: %d sale\n", id);
		b->nPersone+=2;
		b->all_sober=0;
		b->fun_travelling=1;
		DBGpthread_cond_signal(&b->cond_wait_for_entrance, "Sveglia funivia!");
		/*poi vado in wait così non rompo più le scatole finché la funivia non ritorna*/
		DBGpthread_cond_wait(&b->cond_wait_for_end_of_travel, &b->mutex, "Wait for funivia to finish");
		printf("UBRIACO id: %d grida di gioia ed esce\n",id);
 		DBGpthread_mutex_unlock(&b->mutex, "Lock ubriaco");
	}
}

void sobrio(buffer_t *b, int identifier)
{
	int id = identifier;
	while(1){
		DBGpthread_mutex_lock(&b->mutex, "Lock sobrio");
		while(b->fun_travelling || b->nPersone >= 2){
			DBGpthread_cond_wait(&b->cond_wait_for_end_of_travel, &b->mutex, "Wait for funivia to finish");
		}
		/*se esco dalla wait vuol dire che sicuramnete la funivia non sta viaggiando e le persone sono minori di 2*/
		printf("SOBRIO id: %d sale\n", id);
		b->nPersone+=1;
		if(b->nPersone>=2){
			b->all_sober=1;
			b->fun_travelling=1;
			DBGpthread_cond_signal(&b->cond_wait_for_entrance, "Sveglia funivia!");
		}
		/*poi vado in wait così non rompo più le scatole finché la funivia non ritorna*/
		DBGpthread_cond_wait(&b->cond_wait_for_end_of_travel, &b->mutex, "Wait for funivia to finish");
		printf("SOBRIO id: %d grida di gioia ed esce\n",id);
 		DBGpthread_mutex_unlock(&b->mutex, "Lock sobrio");
	}
}

void funivia(buffer_t *b)
{
	while(1){
		DBGpthread_mutex_lock(&b->mutex, "Lock funivia");
		while(b->nPersone < 2){
			DBGpthread_cond_wait(&b->cond_wait_for_entrance, &b->mutex, "Wait for people to enter");
		}
		if(b->all_sober){
			printf("Sono saliti tutti sobri\n");
		}else{
			printf("Sono saliti tutti ubriachi\n");
		}
		DBGpthread_mutex_unlock(&b->mutex, "Lock funivia");
		printf("La funivia parte e fa il suo viaggio\n");
		DBGsleep(2, "Sleep viaggio");
		DBGpthread_mutex_lock(&b->mutex, "Lock funivia");
		printf("La funivia finisce il suo viaggio\n");
		fflush(stdout);
		/*arrivo, quindi tolgo il flag del viaggio e dico a tutti di (eventualemtne ) svegliarsi */
		b->fun_travelling=0;
		b->nPersone=0;
		DBGpthread_cond_broadcast(&b->cond_wait_for_end_of_travel, "Travel ended");
 		DBGpthread_mutex_unlock(&b->mutex, "Lock funivia");
	}	
}


int main() {
    int shmfd, rc,i;
    pid_t pid;
    buffer_t *buffer;
    int shared_seg_size = sizeof(buffer_t);
    pthread_mutexattr_t mattr;
    pthread_condattr_t cvattr;

    /* shmfd = shm_open( "/pedala", O_CREAT | O_EXCL | O_RDWR, S_IRWXU ); */
    shmfd = shm_open( "/pedala", O_CREAT /*| O_EXCL*/ | O_RDWR,         S_IRWXU );
    if (shmfd < 0) {
        perror("In shm_open()");
        exit(1);
    }
    /* adjusting mapped file size (make room for the whole segment to map) */
    rc = ftruncate(shmfd, shared_seg_size);
    if (rc != 0) {
        perror("ftruncate() failed");
        exit(1);
    }

    buffer = (buffer_t *)mmap(NULL, sizeof(buffer_t),
        PROT_READ|PROT_WRITE, MAP_SHARED, shmfd, 0);
    buffer->all_sober = 0;
    buffer->fun_travelling=0;
    buffer->nPersone=0;

    rc=pthread_mutexattr_init(&mattr);
    if( rc ) PrintERROR_andExit(rc,"pthread_mutexattr_init  failed");
    rc=pthread_mutexattr_setpshared(&mattr,PTHREAD_PROCESS_SHARED);
    if( rc ) PrintERROR_andExit(rc,"pthread_mutexattr_setpshared  failed");
    rc=pthread_mutex_init(&buffer->mutex, &mattr);
    if( rc ) PrintERROR_andExit(rc,"pthread_mutex_init  failed");

    rc=pthread_condattr_init(&cvattr);
    if( rc ) PrintERROR_andExit(rc,"pthread_condattr_init  failed");
    rc=pthread_condattr_setpshared(&cvattr,PTHREAD_PROCESS_SHARED);
    if( rc ) PrintERROR_andExit(rc,"pthread_condattr_setpshared  failed");

    rc=pthread_cond_init(&buffer->cond_wait_for_entrance, &cvattr);
    if( rc ) PrintERROR_andExit(rc,"pthread_cond_init  failed");
    rc=pthread_cond_init(&buffer->cond_wait_for_end_of_travel, &cvattr);
    if( rc ) PrintERROR_andExit(rc,"pthread_cond_init  failed");   


   	for(i=0;i<4;i++){
   		pid=fork();
	    if(pid<0) { 
        	PrintERROR_andExit(errno,"fork() failed ");
	    }
	    else if ( pid == 0){/* figlio */
	        sobrio(buffer, i+1);
	   	}
	}

   	for(i=0;i<2;i++){
   		pid=fork();
	    if(pid<0) { 
        	PrintERROR_andExit(errno,"fork() failed ");
	    }
	    else if ( pid == 0){/* figlio */
	        ubriaco(buffer, i+1);
	   	}
	}

	pid=fork();
    if(pid<0) { 
    	PrintERROR_andExit(errno,"fork() failed ");
    }
    else if ( pid == 0){/* figlio */
        funivia(buffer);
   	}
   	

    return(0);
}





