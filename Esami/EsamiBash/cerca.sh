#!/bin/bash

for name in `find /usr/include/linux/netfilter/ -name "*.h"`; do
	output=`cat "$name" | grep int`
	if (( $? !=0 )); then
		echo $name
	fi
done
