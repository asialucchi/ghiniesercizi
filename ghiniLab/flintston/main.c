/* acrossProcessBoundary.c  
   4-17 Synchronization Across Process Boundaries
   a causa dell'uso di strerror_r, usato in printerror.h,
   compilare con -D_POSIX_C_SOURCE >= 200112L
*/

/* messo prima perche' contiene define _POSIX_C_SOURCE */
#include "printerror.h"
#include "DBGpthread.h"


#include <stdlib.h>
#include <unistd.h>   /* exit() etc */
#include <stdio.h>
#include <string.h>     /* per strerror_r  and  memset */
#include <sys/mman.h>  /* Memory MANagement: shm_* stuff, and mmap() */
#include <sys/types.h>
#include <signal.h>
#include <sys/time.h>	/* timeval{} for select() */
#include <time.h>	/* timespec{} for pselect() */
#include <limits.h>	/* for OPEN_MAX */
#include <errno.h>
#include <sys/wait.h>
#include <assert.h>
#include <pthread.h>
#include <semaphore.h>


/*
#include <sys/ipc.h>
#include <sys/shm.h>
*/
#include <fcntl.h>
#include <sys/stat.h>

typedef struct { 
     char buf; 		/* buffer di scambio */
     int lato_dinosauro;
     int postoA;
     int postoB;
     int sceso;
     pthread_mutex_t mutex; 
     pthread_cond_t cond_wait_discesa; 
     pthread_cond_t cond_dormi_dinosauro; /*inizializzare */
     pthread_cond_t cond_sali; } 
buffer_t; 



void cavernicolo(buffer_t *b, int lato, int identifier)
{
	int mio_lato = lato;
	while(1){
	    DBGpthread_mutex_lock(&b->mutex, "cavernicolo lock");
	    
	    while(mio_lato != b->lato_dinosauro){
	    	DBGpthread_cond_wait(&b->cond_sali, &b->mutex, "Wait del cavernicolo se non nel lato giusto");
	    }
	    /*è il mio lato provo a salire */
	    if(b->postoA == -1){
	    	b->postoA = identifier;
	    	printf("Cavernicolo %d sale sul dinosauro sul PostoA nel lato %d\n", identifier, mio_lato);
	    	fflush(stdout);

	    	if(b->postoB != -1){ /*anche l'altro posto è occupato, sveglio il dinosauro */
	    		DBGpthread_cond_signal(&b->cond_dormi_dinosauro, "Risveglio dinosauro");
	    	}	

	    	/*aspetto mi trasporti*/
    		DBGpthread_cond_wait(&b->cond_wait_discesa, &b->mutex, "Attesa trasporto cavernicolo");
    		/*si sveglia e ha il suo da fare*/
    		if(mio_lato==0){
    			mio_lato=1;
    		}else{
    			mio_lato=0;
    		}
    		b->sceso+=1;
	    	printf("Cavernicolo %d è ora nel lato %d\n", identifier, mio_lato);
	    	fflush(stdout);
    		if(b->sceso>=2){
    			b->sceso=0;
    			DBGpthread_cond_signal(&b->cond_dormi_dinosauro, "Risveglio dinosauro");
    		}
	    	DBGpthread_mutex_unlock(&b->mutex, "cavernicolo unlock");
	    	DBGsleep(4, "Sleep cavernicolo");
	    	DBGpthread_mutex_lock(&b->mutex, "cavernicolo lock");
	    }
	    else if(b->postoB == -1){
	    	b->postoB = identifier;
	    	printf("Cavernicolo %d sale sul dinosauro sul PostoB nel lato %d\n", identifier, mio_lato);
	    	fflush(stdout);
	    	if(b->postoA != -1){ /*anche l'altro posto è occupato, sveglio il dinosauro */
	    		DBGpthread_cond_signal(&b->cond_dormi_dinosauro, "Risveglio dinosauro");
	    	}	
	    	
	    	/*aspetto mi trasporti*/
    		DBGpthread_cond_wait(&b->cond_wait_discesa, &b->mutex, "Attesa trasporto cavernicolo");
    		/*si sveglia e ha il suo da fare*/
    		if(mio_lato==0){
    			mio_lato=1;
    		}else{
    			mio_lato=0;
    		}
    		b->sceso+=1; 		
	    	printf("Cavernicolo %d è ora nel lato %d\n", identifier, mio_lato);
	    	fflush(stdout);
    		if(b->sceso>=2){
    			b->sceso=0;
    			DBGpthread_cond_signal(&b->cond_dormi_dinosauro, "Risveglio dinosauro");
    		}	
	    	DBGpthread_mutex_unlock(&b->mutex, "cavernicolo unlock");
	    	DBGsleep(4, "Sleep cavernicolo");
	    	DBGpthread_mutex_lock(&b->mutex, "cavernicolo lock");
	    }

	    DBGpthread_mutex_unlock(&b->mutex, "cavernicolo unlock");
	}
}

void dinosauro(buffer_t *b)
{
	while(1){
	    DBGpthread_mutex_lock(&b->mutex ,"dinosauro lock");
	    while(b->postoA == -1 || b->postoB == -1){ /*ancora un posto non è stato occupato, controllo forse superfluo*/
	    	printf("Dinosauro dorme\n");
	    	fflush(stdout);	
	    	DBGpthread_cond_wait(&b->cond_dormi_dinosauro, &b->mutex, "Dinosauro dorme");    
	    }
	    /*dinosauro ha entrambi i posti occupati*/
	    printf("Dinosauro sposta i cavernicoli\n");
	    fflush(stdout);
	    DBGsleep(2, "Sleep spostamento");
	    if(b->lato_dinosauro==0){
	    	b->lato_dinosauro=1;
	    }else{
	    	b->lato_dinosauro=0;
	    }
	    /*dinosauro sveglia i passegeri dicendo di scendere*/
	    printf("Dinosauro avvisa del trasporto terminato\n");
	    fflush(stdout);
	    DBGpthread_cond_broadcast(&b->cond_wait_discesa, "Dinosauro avvisa di averli trasportati");
	    /*dinosauro aspetta scendano tutti */
    	DBGpthread_cond_wait(&b->cond_dormi_dinosauro, &b->mutex, "Dinosauro dorme");
    	printf("Tutti sono scesi\n");
	    fflush(stdout);
    	/*sono scesi tutti*/
    	b->postoA=-1;
    	b->postoB=-1;
  		/*aspetta salgano tutti*/
  		DBGpthread_cond_broadcast(&b->cond_sali, "Sveglio tutti quelli che aspettavano");
	    DBGpthread_mutex_unlock(&b->mutex, "dinosauro unlock");
	}	
}


int main() {
    int shmfd, rc,i;
    pid_t pid;
    buffer_t *buffer;
    int shared_seg_size = sizeof(buffer_t);
    pthread_mutexattr_t mattr;
    pthread_condattr_t cvattr;

    /* shmfd = shm_open( "/pedala", O_CREAT | O_EXCL | O_RDWR, S_IRWXU ); */
    shmfd = shm_open( "/pedala", O_CREAT /*| O_EXCL*/ | O_RDWR,         S_IRWXU );
    if (shmfd < 0) {
        perror("In shm_open()");
        exit(1);
    }
    /* adjusting mapped file size (make room for the whole segment to map) */
    rc = ftruncate(shmfd, shared_seg_size);
    if (rc != 0) {
        perror("ftruncate() failed");
        exit(1);
    }

    buffer = (buffer_t *)mmap(NULL, sizeof(buffer_t),
        PROT_READ|PROT_WRITE, MAP_SHARED, shmfd, 0);
    buffer->lato_dinosauro = 0;
    buffer->postoA=-1;
    buffer->postoB=-1;
    buffer->sceso=0;

    rc=pthread_mutexattr_init(&mattr);
    if( rc ) PrintERROR_andExit(rc,"pthread_mutexattr_init  failed");
    rc=pthread_mutexattr_setpshared(&mattr,PTHREAD_PROCESS_SHARED);
    if( rc ) PrintERROR_andExit(rc,"pthread_mutexattr_setpshared  failed");
    rc=pthread_mutex_init(&buffer->mutex, &mattr);
    if( rc ) PrintERROR_andExit(rc,"pthread_mutex_init  failed");

    rc=pthread_condattr_init(&cvattr);
    if( rc ) PrintERROR_andExit(rc,"pthread_condattr_init  failed");
    rc=pthread_condattr_setpshared(&cvattr,PTHREAD_PROCESS_SHARED);
    if( rc ) PrintERROR_andExit(rc,"pthread_condattr_setpshared  failed");

    rc=pthread_cond_init(&buffer->cond_wait_discesa, &cvattr);
    if( rc ) PrintERROR_andExit(rc,"pthread_cond_init  failed");
    rc=pthread_cond_init(&buffer->cond_sali, &cvattr);
    if( rc ) PrintERROR_andExit(rc,"pthread_cond_init  failed");   
    rc=pthread_cond_init(&buffer->cond_dormi_dinosauro, &cvattr);
    if( rc ) PrintERROR_andExit(rc,"pthread_cond_init  failed");


   	for(i=0;i<2;i++){
   		pid=fork();
	    if(pid<0) { 
        	PrintERROR_andExit(errno,"fork() failed ");
	    }
	    else if ( pid == 0){/* figlio */
	        cavernicolo(buffer, 0, i+1);
	   	}
	}

	pid=fork();
    if(pid<0) { 
    	PrintERROR_andExit(errno,"fork() failed ");
    }
    else if ( pid == 0){/* figlio */
        cavernicolo(buffer, 1, 3);
   	}

	pid=fork();
    if(pid<0) { 
    	PrintERROR_andExit(errno,"fork() failed ");
    }
    else if ( pid == 0){ /* figlio */
        dinosauro(buffer);
   	}   	


    return(0);
}





