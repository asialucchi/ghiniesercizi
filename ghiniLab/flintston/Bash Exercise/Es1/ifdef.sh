#!/bin/bash
rm -rf temp.txt
for name in `find /usr/include -maxdepth 1 -name "*.h"`; do
	output=`cat "$name" | grep -c "ifdef"`
	if (( "$output" >= 10 )) ; then
		output=`cat "$name" | grep "ifdef"`
		output=${output//#/\\n#}
		echo -e $output > a.txt
		head -n 6 a.txt >> temp.txt
	fi 
done
rm -rf a.txt
sort -bdi temp.txt | grep -v "^$" > FINALE.txt
rm -rf temp.txt