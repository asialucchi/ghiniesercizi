/* file:  NProd_NCons.c 
   NProduttori e NConsumatori che si scambiano prodotti
   mediante un unico Buffer. E' la versione efficente perche' 
   - utilizza due pthread_cond_var (una per Prod e una per Cons)
   - sveglia i pthread solo quando hanno qualcosa da fare.
*/ 

#ifndef _THREAD_SAFE
	#define _THREAD_SAFE
#endif
#ifndef _POSIX_C_SOURCE
	#define _POSIX_C_SOURCE 200112L
#endif


#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <pthread.h> 
#include "printerror.h"
#include "DBGpthread.h"

/* settati per vedere output di debug
#define DEBUGCONTROLLARIEMPIMENTOBUFFER
#define DEBUGSTAMPAAZIONI
#define DEBUGSTAMPASTATOBUFFER
*/

#define NUMPRODA 3
#define NUMPRODB 5
#define NUMCONS 10 
#define NUMBUFFER 1	/* NON MODIFICARE MAI */

/* dati da proteggere */
uint64_t valGlobaleA=0;
uint64_t valGlobaleB=0;

/* variabili per la sincronizzazione */
pthread_mutex_t  mutex; 
pthread_cond_t   condProdA, condCons, condProdB; 
int numBufferAPieni=0; /* 0 o 1 */
int numBufferBPieni=0;
int numProdAWaiting=0;
int numProdAWaitingAndSignalled=0; /* deve essere <= #buffer vuoti */
int numProdBWaiting=0;
int numProdBWaitingAndSignalled=0; /* deve essere <= #buffer vuoti */
int numConsWaiting=0;
int numConsWaitingAndSignalled=0; /* deve essere <= #buffer pieni */

void *ProduttoreA (void *arg) 
{ 
	char Plabel[128];
	char Plabelsignal[128];
	int indice;
	uint64_t valProdotto=0;

	indice=*((int*)arg);
	free(arg);
	sprintf(Plabel,"P%d",indice);
	sprintf(Plabelsignal,"P%d->C",indice);

	while(1) {
		valProdotto++;
		
		DBGpthread_mutex_lock(&mutex,Plabel); 
		if( numProdAWaitingAndSignalled >= (NUMBUFFER-numBufferAPieni)){
			numProdAWaiting++;
			DBGpthread_cond_wait(&condProdA,&mutex,Plabel);
			numProdAWaiting--;
			numProdAWaitingAndSignalled--;
		}
		valGlobaleA=valProdotto;

		printf("ProdA %s inserisce %lu \n", Plabel, valGlobaleA ); 
		fflush(stdout);

		numBufferAPieni++;
		if( 
		    (numProdBWaitingAndSignalled < numProdBWaiting ) 
			&&
		    (numProdBWaitingAndSignalled < (NUMBUFFER-numBufferBPieni)) 
		  )  {
			/* risveglio un Produttore B per riempire un buffer */
			DBGpthread_cond_signal(&condProdB,Plabelsignal); 
			numProdBWaitingAndSignalled++;
		}

		DBGnanosleep(500000000, "Errore nano Sleep");
		/* rilascio mutua esclusione */
		DBGpthread_mutex_unlock(&mutex,Plabel); 
	}
	pthread_exit(NULL); 
} 

void *ProduttoreB (void *arg) 
{ 
	char Plabel[128];
	char Plabelsignal[128];
	int indice;
	uint64_t valProdotto=0;

	indice=*((int*)arg);
	free(arg);
	sprintf(Plabel,"P%d",indice);
	sprintf(Plabelsignal,"P%d->C",indice);

	while(1) {
		valProdotto++;
		
		DBGpthread_mutex_lock(&mutex,Plabel); 
		if( numProdBWaitingAndSignalled >= (NUMBUFFER-numBufferBPieni)){
			numProdBWaiting++;
			DBGpthread_cond_wait(&condProdB,&mutex,Plabel);
			numProdBWaiting--;
			numProdBWaitingAndSignalled--;
		}
		valGlobaleB=valProdotto;

		printf("ProdB %s inserisce %lu \n", Plabel, valGlobaleB ); 
		fflush(stdout);

		numBufferBPieni++;
		if( 
			( numConsWaitingAndSignalled < numConsWaiting ) 
			&&
			( numConsWaitingAndSignalled < numBufferBPieni ) 
		  ) {
			/* risveglio un Consumatore per svuotare un buffer */
			DBGpthread_cond_signal(&condCons,Plabelsignal); 
			numConsWaitingAndSignalled++;
		}

		/* rilascio mutua esclusione */
		DBGnanosleep(500000000, "Errore nano Sleep");
		DBGpthread_mutex_unlock(&mutex,Plabel); 
	}
	pthread_exit(NULL); 
}


void *Consumatore (void *arg) 
{ 
	uint64_t valA;
	uint64_t valB;
	char Clabel[128];
	char Clabelsignal[128];
	int indice;

	indice=*((int*)arg);
	free(arg);
	sprintf(Clabel,"C%d",indice);
	sprintf(Clabelsignal,"C%d->P",indice);

	
	while(1) {
		DBGpthread_mutex_lock(&mutex,Clabel); 
		if( numConsWaitingAndSignalled >= numBufferBPieni ) {
			numConsWaiting++;
			DBGpthread_cond_wait(&condCons,&mutex,Clabel);
			numConsWaiting--;
			numConsWaitingAndSignalled--;
		}

		/* prendo cio' che c'e' nel buffer */
		valA=valGlobaleA;
		valB=valGlobaleB;

		printf("Cons %s legge BufA %lu e BufB %lu \n", Clabel, valA,valB ); 
		fflush(stdout);

		numBufferBPieni--;
		numBufferAPieni--;
		if( 
		    (numProdAWaitingAndSignalled < numProdAWaiting ) 
			&&
		    (numProdAWaitingAndSignalled < (NUMBUFFER-numBufferAPieni)) 
		  ) 
		{ 
			/* risveglio 1 ProdA per riempire il buffer svuotato */
			DBGpthread_cond_signal(&condProdA,Clabelsignal); 
			numProdAWaitingAndSignalled++;
		}

		DBGnanosleep(500000000, "Errore nano Sleep");
		DBGpthread_mutex_unlock(&mutex,Clabel); 

	}
	pthread_exit(NULL); 
} 

int main (int argc, char* argv[] ) 
{ 
	pthread_t    th; 
	int  i, *intptr;

	DBGpthread_cond_init(&condProdA, NULL,"pthread_cond_initA failed");
	DBGpthread_cond_init(&condProdB, NULL,"pthread_cond_initB failed");
	DBGpthread_cond_init(&condCons, NULL,"pthread_cond_init failed");
	DBGpthread_mutex_init(&mutex,NULL,"pthread_mutex_init failed");

	/* all'inizio i Cons devono aspettare il primo Prod */
	numBufferAPieni=0; /* 0 o 1 */
	numProdAWaiting=0;
	numProdAWaitingAndSignalled=0;
	numBufferBPieni=0; /* 0 o 1 */
	numProdBWaiting=0;
	numProdBWaitingAndSignalled=0;
	numConsWaiting=0;
	numConsWaitingAndSignalled=0;

	for(i=0;i<NUMCONS;i++) {
		intptr=malloc(sizeof(int));
		if( !intptr ) { printf("malloc failed\n");exit(1); }
		*intptr=i;
		pthread_create( &th,NULL,Consumatore,(void*)intptr);
	}

	for(i=0;i<NUMPRODA;i++) {
		intptr=malloc(sizeof(int));
		if( !intptr ) { printf("malloc failed\n");exit(1); }
		*intptr=i;
		pthread_create( &th,NULL,ProduttoreA,(void*)intptr); 
	}

	for(i=0;i<NUMPRODB;i++) {
		intptr=malloc(sizeof(int));
		if( !intptr ) { printf("malloc failed\n");exit(1); }
		*intptr=i;
		pthread_create( &th,NULL,ProduttoreB,(void*)intptr); 
	}

	pthread_exit(NULL); 

	return(0); 
} 
  
  
  
