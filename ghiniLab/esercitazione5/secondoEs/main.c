#include <stdio.h>
#include "alloca.h"

int main(void){
	int * punt;
	int i;
	int begin=-19;
	ALLOCMACRO(punt);
	printf("\n");
	if(punt!=NULL){
		for(i=0;i<10;i++){
			punt[i]=begin + i;
			printf("%d ",punt[i]);
		}
	}
	return 0;
}
