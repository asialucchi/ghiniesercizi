/* file:  CondVarSignal.c 
   Routine che fornisce un synchronization point. 
   E' chiamata da ognuno dei SYNC_MAX pthread, che si fermano 
   finche' tutti gli altri sono arrivati allo stesso punto di esecuzione. 
*/ 

#define _THREAD_SAFE

#include <unistd.h> 
#include <stdlib.h> 
#include <stdio.h> 
#include <stdint.h>
#include <inttypes.h>
#include <pthread.h> 
#include "printerror.h"
#include "DBGpthread.h"

#define SYNC_MAX 5 

pthread_mutex_t  sync_lock; 
pthread_cond_t   sync_cond; 
int  sync_count = 0; 
intptr_t contatore=0;

void SyncPoint(intptr_t threadNumber) 
{ 
	/* blocca l'accesso al counter */ 
	DBGpthread_mutex_lock(&sync_lock,"pthread_mutex_lock failed"); /* no EINTR */
  
	/* incrementa il counter di quelli arrivati*/ 
	sync_count++; 

	/* controlla se deve aspettare o no */ 
	if (sync_count < SYNC_MAX) {
		/* aspetta */ 
		DBGpthread_cond_wait(&sync_cond, &sync_lock,"pthread_cond_wait failed"); /* no EINTR */
		DBGpthread_cond_signal (&sync_cond , "signal"); /* senza questa signal ne terminano solo 2 ,"pthread_cond_signal failed");*/
	}
	else    {
		/* tutti hanno raggiunto il punto di barriera */ 
		DBGpthread_cond_broadcast(&sync_cond,"pthread_cond_broadcast failed"); /* no EINTR */
	}

	while(contatore!=threadNumber){
		DBGpthread_cond_wait(&sync_cond, &sync_lock, "Bloccato perché non quello giusto");
	}
	/* sblocca il mutex */
	printf("In uscita il thread %" PRIdPTR "\n", threadNumber); 
	contatore++;
	DBGpthread_cond_broadcast(&sync_cond,"pthread_cond_broadcast failed"); /* no EINTR */
	DBGpthread_mutex_unlock (&sync_lock, "pthread_mutex_unlock failed");  /* senza unlock ne termina solo 1 ,"pthread_mutex_unlock failed"); */
	return; 
} 

void *Thread (void *arg) 
{ 
	pthread_t  th; 

	th=pthread_self();    /* thread identifier */ 
	printf ("%" PRIdPTR "\n", (intptr_t)arg); 
	SyncPoint((intptr_t) arg); 
	printf("Sono %lu e sono uscito \n", th); 
	pthread_exit(NULL); 
} 

int main () 
{ 
	pthread_t    th[SYNC_MAX]; 
	intptr_t i;

	DBGpthread_cond_init(&sync_cond, NULL,"pthread_cond_init failed"); /* no EINTR */
	DBGpthread_mutex_init(&sync_lock, NULL,"pthread_mutex_init failed"); /* no EINTR */

	for(i=0;i<SYNC_MAX;i++) {
		pthread_create(&(th[i]), NULL, Thread, (void *)i); 
	}
	pthread_exit (NULL); 
} 
  
  
  
