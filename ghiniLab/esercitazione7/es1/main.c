/* banale_contrucco.c  */

 
#define _THREAD_SAFE
#define _REENTRANT



#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <stdint.h>	/* uintptr_t */
#include <inttypes.h>	/* PRIiPTR */
#include <pthread.h>
#include <string.h>

#define NUM_THREADS 100

int N=-1;

void *PrintHello(void *arg)
{
	/* sleep(4); */
	printf("\n %"  PRIiPTR ": Hello World! N=%d\n", (intptr_t)arg, N );
	pthread_exit (NULL);
}

int main()
{
	pthread_t tid[1000];
	int rc;
	char buf[500];
	intptr_t t;
	while(1){
		for(t=0;t < NUM_THREADS;t++){

			printf("Creating thread %" PRIiPTR "\n", t);
			rc = pthread_create (&tid[t], NULL, PrintHello, (void*)t );
			if (rc){
				printf("ERROR; return code from pthread_create() is %i \n",rc);
				printf("\n\nDetails about error\n");
				fflush(stdin);
				strerror_r(rc, &buf[0], 500);
				printf("%s\n", buf);
				exit(1);
			}
		}

		for(t=0;t < NUM_THREADS;t++){	
			pthread_join(tid[t], NULL);
		}
	}
	return(0);
}