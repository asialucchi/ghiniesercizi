#define _THREAD_SAFE

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>


/*fixed*/
void * functionMultiThread(void * index){
	sleep(5);
	printf("%g\n", *((double *) index));
	//free?
	pthread_exit(NULL);
}


int main(void){
	unsigned int * number;
	int i;
	pthread_t identifiers[10];
	number=malloc(sizeof(unsigned int));
	*number=1;
	for(i=0;i<10;i++){
		double *p;
		p = malloc(sizeof(double));
		*p = (double) rand_r(number);
		pthread_create(&identifiers[i],NULL, functionMultiThread, (void *)p);
	}
	pthread_exit(NULL);
}