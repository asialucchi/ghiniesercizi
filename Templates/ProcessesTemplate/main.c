#define _POSIX_C_SOUCE 200112L
#define _THREAD_SAFE
#define _REENTRANT
#define _GNU_SOURCE

#include <stdlib.h>
#include <unistd.h>   /* exit() etc */
#include <stdio.h>
#include <string.h>     /* per strerror_r  and  memset */
#include <sys/mman.h>  /* Memory MANagement: shm_* stuff, and mmap() */
#include <sys/types.h>
#include <signal.h>
#include <sys/time.h>	/* timeval{} for select() */
#include <time.h>	/* timespec{} for pselect() */
#include <limits.h>	/* for OPEN_MAX */
#include <errno.h>
#include <sys/wait.h>
#include <assert.h>
#include <pthread.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/stat.h>

#include "lib/printerror.h"
#include "lib/DBGpthread.h"


typedef struct {
	int campo1;
	char campo2;
	/*
	*
	*
	*/
	pthread_mutex_t mutex;
	pthread_cond_t cond;
}buffer;

void Son(buffer *b, int index) {
	printf("I'm the son %d\n",index );
}

void Father(buffer *b) {
	printf("I'm the father\n");
}


int main() {
	int shmfd;
	int shared_seg_size = sizeof(buffer);
	pid_t pid = 0;
	buffer *b;
	int i = 0;
	pthread_mutexattr_t mattr;
	pthread_condattr_t cattr;

	shmfd = shm_open("/temp", O_CREAT | O_EXCL | O_RDWR, S_IRWXU);
	if(shmfd < 0) {
		printf("Error while creating the shared memory file\n");
		exit(1);
	}

	if (ftruncate(shmfd,shared_seg_size) != 0) {
		printf("Error\n");
		exit(1);
	}

	b = (buffer *)mmap(NULL, sizeof(buffer),  PROT_READ|PROT_WRITE, MAP_SHARED, shmfd, 0);

	/*initiate the buffer variables*/
	b->campo1 = 0;

	pthread_mutexattr_init(&mattr);
	pthread_mutexattr_setpshared(&mattr,PTHREAD_PROCESS_SHARED);
	DBGpthread_mutex_init(&b->mutex, &mattr, "Error while initializing the mutex");

	pthread_condattr_init(&cattr);
	pthread_condattr_setpshared(&cattr, PTHREAD_PROCESS_SHARED);
	DBGpthread_cond_init(&b->cond, &cattr, "Error while initializing the cond");

	for (i = 0; i < 2; ++i) {
		pid = fork();
		if(pid < 0) {
			printf("Error while forking the process\n");
			exit(1);
		} else if(pid == 0) {/*son process*/
			Son(b, (i + 1));/*son function*/
			return 0; /*if the process must terminate after the son function execution*/
		}
	}
	/*other processes creation*/


	/*father function*/
	Father(b);
	if(shm_unlink("/temp") < 0) {
		printf("Error while deleting the file\n");
		exit(1);
	}

	return 0;
}
